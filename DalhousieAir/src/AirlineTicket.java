/**
 * Represents a Ticket information for a customer in a Dalhousie Airline.
 */
public class AirlineTicket {
	//Declaration
	private Customer passenger;
	private Flight flight;
	private double price;
	
	/**
	 * Constructor to initialize airline ticket
	 */
	public AirlineTicket() {

	}
	/**
	 * Constructor to initialize airline ticket and set its member
	 * @param passenger - Customer Info
	 * @param flight - Flight Info
	 * @param price - Ticket Price
	 */
	public AirlineTicket(Customer passenger, Flight flight, double price) {
		this.passenger = passenger;
		this.flight = flight;
		this.price = price;
	}
	/**
	 * Returns String representation of AirlineTicket Object
	 */
	@Override
	public String toString() {
		return passenger.getName() + ", Flight " + flight.getFlightNumber() + ", " + flight.getOrigin() + " to " +
				flight.getDestination() + ", " + flight.getDeparture() + ", $" + price;
	}

	protected void setPassenger(Customer passenger) {
		this.passenger = passenger;
	}

	public Customer getPassenger() {
		return passenger;
	}

	protected void setFlight(Flight flight) {
		this.flight = flight;
	}

	public Flight getFlight() {
		return flight;
	}

	protected void setPrice(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}
}
