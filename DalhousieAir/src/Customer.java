/**
 * Represents a Customer information in a Dalhousie Airline.
 */
public class Customer {
	//Declaration
	private String name;
	private String address;
	private int membershipNumber;
	private int membershipPoints;

	/**
	 * Constructor to initialize customer
	 */
	public Customer() {

	}	
	/**
	 * Constructor to initialize customer and sets its member 
	 * @param name - Customer name
	 * @param address - Customer address
	 * @param membershipNumber - customer membership number 
	 */
	public Customer(String name, String address, int membershipNumber) {
		this.membershipPoints = 0;
	}
	/**
	 * Returns String representation of Customer Object
	 * @return - Returns customer info
	 */
	@Override
	public String toString() {
		return name + ", " + address + ", #" + membershipNumber
				+ ", " + membershipPoints + " points";
	}
	/**
	 * Calculates points based on ticket amount and adds it to accumulated points
	 * @airlineTicket - Ticket details of a customer
	 * @return - Accumulated mileage points 
	 */
	public int applyPoints(AirlineTicket airlineTicket) {		
		return membershipPoints + ((int) Math.round(airlineTicket.getPrice() / 100));				
	}

	protected void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	protected void setAddress(String address) {
		this.address = address;
	}

	public String getAddress() {
		return address;
	}

	protected void setMembershipNumber(int membershipNumber) {
		this.membershipNumber = membershipNumber;
	}

	public int getMembershipNumber() {
		return membershipNumber;
	}

	protected void setMembershipPoints(int membershipPoints) {
		this.membershipPoints = membershipPoints;
	}

	public int getMembershipPoints() {
		return membershipPoints;
	}
}
