/**
 * Represents a Flight information for a customer in a Dalhousie Airline.
 */
public class Flight {
	//Declaration
	private int flightNumber;
	private String origin;
	private String destination;
	private String departure;

	/**
	 * Constructor to initialize Flight
	 */
	public Flight() {

	}
	/**
	 * Constructor to initialize flight and can be used to initialize its member
	 * @param flightNumber - Flight number 
	 * @param origin - Origin where customer departs
	 * @param destination - destination where customer arrives
	 * @param departure - Departure time
	 */
	public Flight(int flightNumber, String origin, String destination, String departure) {

	}
	/**
	 * Returns String representation of Flight Object
	 */
	@Override
	public String toString() {
		return "Flight " + flightNumber + ", " + origin + " to " + destination
				+ ", " + departure;
	}

	protected void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	protected void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getOrigin() {
		return origin;
	}

	protected void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestination() {
		return destination;
	}

	protected void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDeparture() {
		return departure;
	}
}
