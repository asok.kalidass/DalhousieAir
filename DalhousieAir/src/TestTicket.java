
/**
 * Represents a Dalhousie Airline and tests information about customer's itinerary
 * <p>
 * Display of airline customer itinerary can have following rules
 * <ul>
 * <li> Apply points - divide the price of ticket by 100 and add it to accumulated points
 * <li>
 * </ul>
 * 
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student 
 */
public class TestTicket {
	/**
	 * Customer, Ticket, Point Calculation and Flight info along with its itinerary is tested and displayed.
	 * @param args - Console line argument
	 */
	public static void main(String[] args) {
		System.out.println("The Customer Information is :");
		Customer customer = TestCustomer();
		System.out.println("The Flight Information is :");
		Flight flight = TestFlight();
		System.out.println("The AirlineInformation is :");
		AirlineTicket airlineTicket = TestAirlineTicket(customer, flight);
		//Calculates points for a current air ticket
		System.out.println("The Total Accumulated points is :");
		System.out.println(customer.applyPoints(airlineTicket));		
	}
	/**
	 * Customer info tested and displayed.
	 * @return customer - Returns customer information
	 */
	public static Customer TestCustomer()
	{
		Customer customer = new Customer();
		customer.setName("Asok Kalidass");
		customer.setAddress("1030 South Park Street");
		customer.setMembershipNumber(1818);
		customer.setMembershipPoints(13);
		System.out.println(customer); 
		return customer;
	}
	/**
	 * Flight info for the customer is tested and displayed
	 * @return Flight - Returns flight information
	 */
	public static Flight TestFlight()
	{
		Flight flight = new Flight();
		flight.setOrigin("Calgary");
		flight.setDestination("Halifax");
		flight.setFlightNumber(879);
		flight.setDeparture("01/20/2016 7:50 pm");
		System.out.println(flight);
		return flight;
	}
	/**
	 * Ticket details for the customer is tested and displayed
	 * @param customer - Customer details
	 * @param flight - flight details about customer
	 * @return AirlineTicket - returns ticket information
	 */
	public static AirlineTicket TestAirlineTicket(Customer customer, Flight flight) 
	{
		AirlineTicket airlineTicket = new AirlineTicket(customer, flight, 120);
		System.out.println(airlineTicket);
		return airlineTicket;
	}
}
